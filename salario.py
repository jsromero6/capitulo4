#Mediante el ingreso del número de horas trabajadas
#y la tarifa por hora se obtendrá el salario del trabajador
#crear una función llamada calculo_salario que reciba dos
#parámetros (hora y tarifa)

#author : "Jeffree Romero"
#email: "sebastianloj14@gmail.com"

horas = int(input('Ingrese el número de horas trabajadas: '))
tarifaHora = float(input('Ingrese la tarifa por hora: '))
salario = horas * tarifaHora

def calculo_salario(hora, tarifa):
    if horas > 40:
        horasExtra = horas - 40
        print('El número de horas extras trabajadas es: ', horasExtra)
        salario = (horas - horasExtra) * tarifaHora
        print('El salario base es: ', salario)
        tarifaExtra = (tarifaHora * 1.5) * horasExtra
        print('El valor por horas extras es: ', tarifaExtra)
        salarioExtra = tarifaExtra + salario
        print('El salario total es: ', salarioExtra)
    elif horas <= 40:
        print('El salario total es: ', salario)

calculo_salario(horas, tarifaHora)